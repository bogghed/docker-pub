package se.bogghed.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    private static final Logger logger = LoggerFactory.getLogger(MyController.class);

    @RequestMapping("/concat/{first}/{second}")
    public ResponseEntity<String> concat(@PathVariable String first, @PathVariable String second) {

        String result = first.concat(second);
        logger.info("Concat of {} and {} is {}", first, second, result);
        return ResponseEntity.ok(result);
    }
}
